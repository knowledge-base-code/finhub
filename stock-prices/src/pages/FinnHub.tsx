import * as React from 'react'
import { Typography } from 'antd'
import 'antd/dist/antd.css'

import { QueryClientProvider, QueryClient } from 'react-query'
import { ReactQueryDevtools } from 'react-query/devtools'

import { HomeChart } from '../components/HomeChart'
const { Title } = Typography

const queryClient = new QueryClient()

export const FinnHub: React.FC = () => {

  return (
    <section>
      <Title level={3}>Choose Stocks</Title>
      <QueryClientProvider client={queryClient}>
        <HomeChart />
        <ReactQueryDevtools initialIsOpen />
      </QueryClientProvider>
    </section>
  )
}
