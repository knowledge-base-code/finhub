import * as React from 'react'
import './App.css'
import { FinnHub } from './pages/FinnHub'

function App () {
  return (
    <div className='App'>
      <FinnHub />
    </div>
  )
}

export default App
