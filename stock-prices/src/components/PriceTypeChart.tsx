import { RadioChangeEvent, Space } from 'antd'
import * as React from 'react'
import { PriceType } from './PriceType'
import { IStockChart, StockChart } from './StockChart'

import { QueryClientProvider, QueryClient } from 'react-query'
import { ReactQueryDevtools } from 'react-query/devtools'

interface IPriceTypeChart {
  stocks: string[]
  dates: Object[]
}

const queryClient = new QueryClient()

export const PriceTypeChart: React.FC<IPriceTypeChart> = ({ stocks, dates }) => {
  const [priceType, setPriceType] = React.useState<IStockChart['price']>(() => 'O')

  return (
    <Space>
      <PriceType isVertical onchange={setPriceType as any as (e: RadioChangeEvent) => void} />
      <QueryClientProvider client={queryClient}>
        <StockChart price={priceType} stocks={stocks} dates={dates} />
        <ReactQueryDevtools initialIsOpen />
      </QueryClientProvider>
    </Space>
  )
}
