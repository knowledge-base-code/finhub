import * as React from 'react'
import 'antd/dist/antd.css'
import { Story, Meta } from '@storybook/react'

import { PriceType } from './PriceType'

export default {
  title: 'Chart/PriceType',
  component: PriceType,
  parameters: { actions: { argTypesRegex: '^on.*' } }
} as Meta

export const Default: Story<Meta > = args => <PriceType onchange={() => {}} {...args} />

export const Vertical: Story<Meta> = args => <PriceType onchange={() => {}} isVertical {...args} />
