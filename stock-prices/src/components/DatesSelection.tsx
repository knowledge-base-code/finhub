import * as React from 'react'
import { DatePicker } from 'antd'

const { RangePicker } = DatePicker

interface IDatesSelection {
  onchange?: (stringDates: string[]) => void
}

export const DatesSelection: React.FC<IDatesSelection> = ({ onchange }) => {
  return (
    <RangePicker allowClear={false} onChange={handleOnChange} />
  )

  function handleOnChange (_momentDates: any, stringDates: [string, string]): void {
    console.log({ onChange: stringDates })
    onchange?.(stringDates)
  }
}
