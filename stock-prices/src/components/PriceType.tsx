import * as React from 'react'

import { Radio, RadioChangeEvent, Space } from 'antd'

interface IPriceType {
  isVertical?: boolean
  onchange: (e: RadioChangeEvent) => void
}

export const PriceType: React.FC<IPriceType> = ({ isVertical = false, onchange }) => {
  return (
    <Radio.Group name='pricetype' defaultValue='O' onChange={e => onchange(e.target.value)}>
      <Space direction={isVertical ? 'vertical' : 'horizontal'}>
        <Radio value='O'>Open</Radio>
        <Radio value='H'>High</Radio>
        <Radio value='L'>Low</Radio>
        <Radio value='C'>Close</Radio>
        <Radio value='V'>Volume</Radio>
      </Space>
    </Radio.Group>
  )
}
