import * as React from 'react'

import { Space } from 'antd'
import { DatesSelection } from './DatesSelection'
import { StockSelect, stock } from './StockSelect'

import { useQuery } from 'react-query'

interface IStocksSelect {
  onSelectedStock: React.Dispatch<React.SetStateAction<string[]>>
  onSelectedDates: React.Dispatch<React.SetStateAction<string[]>>
}

async function getStockSymbols () {
  const token = 'c5i4bjaad3i958lqrgq0'
  const stockSymbol: (exchange: string) => string = exchange => `https://finnhub.io/api/v1/stock/symbol?exchange=${exchange}&token=${token}`

  return await fetch(stockSymbol('US'))
    .then(async res => await res.json(), _err => { throw new Error('getStockSymbols') })
}

export const StocksSelect: React.FC<IStocksSelect> = ({ onSelectedStock, onSelectedDates }) => {
  const { isLoading, isError, data, error } = useQuery('stockSymbols', getStockSymbols, {
    retry: false,
    retryDelay: 1000 * 5
  })

  if (isError) {
    console.error({ useQuery: error })
  }

  let symbols: any[] = []
  if (!isLoading && !isError) {
    symbols = (data as stock[]).map(stock => ({ value: stock.symbol, disabled: false }))
  }

  return (
    <Space>
      <StockSelect options={symbols} onchange={handleOnChange(0)} isLoading={isLoading} isError={isError} />
      <StockSelect options={symbols} onchange={handleOnChange(1)} isLoading={isLoading} isError={isError} />
      <StockSelect options={symbols} onchange={handleOnChange(2)} isLoading={isLoading} isError={isError} />
      <DatesSelection onchange={onSelectedDates} />
    </Space>
  )

  function handleOnChange (index: number) {
    return function setStockAtIndex (value: string) {
      onSelectedStock(stocks => {
        const newStocks = [...stocks]
        newStocks[index] = value
        return newStocks
      })
    }
  }
}
