import { Space } from 'antd'
import * as React from 'react'
import { PriceTypeChart } from './PriceTypeChart'
import { StocksSelect } from './StocksSelect'

export const HomeChart: React.FC = () => {
  const [selectedStocks, setSelectedStocks] = React.useState(() => ['AAPL'])
  const [selectedDates, setSelectedDates] = React.useState(() => [] as string[])

  return (
    <Space direction='vertical' align='center' size={40}>
      <StocksSelect onSelectedStock={setSelectedStocks} onSelectedDates={setSelectedDates} />
      <PriceTypeChart stocks={selectedStocks} dates={selectedDates} />
    </Space>
  )
}
