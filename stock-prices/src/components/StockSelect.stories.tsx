import * as React from 'react'
import 'antd/dist/antd.css'
import { Story, Meta } from '@storybook/react'
import { QueryClientProvider, QueryClient } from 'react-query'

import { StockSelect, stock } from './StockSelect'
import stocks from '../mock.10_stockSymbol.US.json'

export default {
  title: 'Stock/Select',
  component: StockSelect,
  parameters: { actions: { argTypesRegex: '^on.*' } },
  decorators: [
    (Story) => {
      const queryClient = new QueryClient()
      return (
        <QueryClientProvider client={queryClient}>
          <Story />
        </QueryClientProvider>
      )
    }
  ]
} as Meta

const options: any[] = (stocks as stock[]).map(stock => ({ value: stock.symbol, disabled: false }))

const Template: Story<Meta> = (args) => <StockSelect {...args} />

export const Full = Template.bind({})
Full.args = {
  options
}

export const Empty = Template.bind({})
Empty.args = {
  options: []
}

export const Large = Template.bind({})
Large.args = {
  options,
  size: 'large'
}

export const Small = Template.bind({})
Small.args = {
  size: 'small',
  options
}
