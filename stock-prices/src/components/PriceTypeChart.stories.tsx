import * as React from 'react'
import 'antd/dist/antd.css'
import { Story, Meta } from '@storybook/react'
import { PriceTypeChart } from './PriceTypeChart'

export default {
  title: 'Screens/PriceTypeChart',
  component: PriceTypeChart,
  parameters: { actions: { argTypesRegex: '^on.*' } }
} as Meta

// export const Default: Story<Meta> = args => <PriceTypeChart {...args} />
export const Default: Story<Meta> = args => <PriceTypeChart {...args} />
Default.args = {
  stocks: []
}

export const OneStock = Default.bind({})
OneStock.args = {
  stocks: ['AAPL']
}

export const TwoStocks = Default.bind({})
TwoStocks.args = {
  stocks: ['AAPL', 'FB']
}

export const ThreeStocks = Default.bind({})
ThreeStocks.args = {
  stocks: ['AAPL', 'FB', 'AMZN']
}
