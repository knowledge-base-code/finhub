import * as React from 'react'
import 'antd/dist/antd.css'
import { Story, Meta } from '@storybook/react'
import { QueryClientProvider, QueryClient } from 'react-query'

import { StocksSelect } from './StocksSelect'

export default {
  title: 'Screens/StocksSelect',
  component: StocksSelect,
  parameters: { actions: { argTypesRegex: '^on.*' } },
  decorators: [
    (Story) => {
      const queryClient = new QueryClient()
      return (
        <QueryClientProvider client={queryClient}>
          <Story />
        </QueryClientProvider>
      )
    }
  ]
} as Meta

export const Default: Story<Meta> = args => <StocksSelect {...args} />
