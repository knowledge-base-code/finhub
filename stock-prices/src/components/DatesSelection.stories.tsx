import * as React from 'react'
import 'antd/dist/antd.css'
import { Story, Meta } from '@storybook/react'

import { DatesSelection } from './DatesSelection'

export default {
  title: 'Dates/Picker',
  component: DatesSelection,
  parameters: { actions: { argTypesRegex: '^on.*' } }
} as Meta

export const Default: Story<Meta> = (args) => <DatesSelection onchange={() => {}} {...args} />
