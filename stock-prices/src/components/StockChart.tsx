import * as React from 'react'

import { Line, LineChart, LineSeries } from 'reaviz'
import chroma from 'chroma-js'
import { useQueries } from 'react-query'

/**
 * Sample of data for Reaviz charts.
 *
export const readyData = [
  {
    key: 'AAPL',
    data: aapl.t.map((t, id) => ({
      key: new Date(t),
      id,
      data: aapl.c[id]
    }))
  },
  {
    key: 'FB',
    data: fb.t.map((t, id) => ({
      key: new Date(t),
      id,
      data: fb.c[id]
    }))
  },
  {
    key: 'AMZN',
    data: amzn.t.map((t, id) => ({
      key: new Date(t),
      id,
      data: amzn.c[id]
    }))
  }
]

 */

export interface IStockChart {
  price: 'O' | 'H' | 'L' | 'C' | 'V'
  stocks: string[]
  dates: Object[]
}

interface IStockCandles {
  symbol: string
  resolution: string
  from: string
  to: string
}

async function stockCandles ({ symbol, resolution = 'D', from, to }: IStockCandles) {
  const token = 'c5i4bjaad3i958lqrgq0'
  const stockCandles = ({ symbol, resolution, from, to }: IStockCandles) => `https://finnhub.io/api/v1/stock/candle?symbol=${symbol}&resolution=${resolution}&from=${from}&to=${to}&token=${token}`

  return await fetch(stockCandles({ symbol, resolution, from, to }))
    .then(async res => await res.json(), _err => { throw new Error('getStockCandles') })
}

const useCandleChart = (price: IStockChart['price'], stocks: IStockChart['stocks'], dates: IStockChart['dates']) => {
  const useQueriesResult = useQueries(
    stocks.map(stock => {
      return {
        queryKey: ['stock', { stock }],
        queryFn: async () => await stockCandles({ symbol: stock, resolution: 'D', from: new Date(dates[0] as string).valueOf() / 1000 + '', to: new Date(dates[1] as string).valueOf() / 1000 + '' }),
        enabled: (dates?.length ?? 0) === 2
      }
    })
  )

  return [stocks.map((stock, stockId) => {
    if (!useQueriesResult[stockId].isSuccess || (useQueriesResult[stockId].data as any).s === 'no_data') {
      return {
        key: stock,
        data: []
      }
    }

    const stockData = useQueriesResult[stockId].data as any
    return {
      key: stock,
      data: stockData.t.map((timestamp: string, id: number) => ({
        key: new Date(timestamp),
        id,
        data: stockData[price.toLowerCase()][id]
      }))
    }
  })]
}

export const StockChart: React.FC<IStockChart> = ({ price = 'O', stocks, dates }) => {
  const [data] = useCandleChart(price, stocks?.filter(x => x) ?? [], dates)

  return (
    <LineChart
      height={300}
      width={600}
      data={data}
      series={
        <LineSeries
          type='grouped'
          line={
            <Line strokeWidth={3} />
	  }
          colorScheme={chroma.scale(['27efb5', '00bfff']).colors(data?.length ?? 0)}
        />
      }
    />
  )
}
