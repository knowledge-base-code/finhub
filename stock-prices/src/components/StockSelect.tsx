import { Select } from 'antd'

export interface stock {
  currency: string
  description: string
  displaySymbol: string
  symbol: string
}

interface IStockSelect {
  options: Array<{ value: stock['symbol'], disabled?: boolean}>
  size?: 'large' | 'middle' | 'small'
  onchange: (value: string) => void
  isLoading: boolean
  isError: boolean
}

export const StockSelect: React.FC<IStockSelect> = ({ options, size = 'middle', onchange, isLoading, isError }) => {
  return (
    <Select
      showSearch
      allowClear
      {...isLoading ? { loading: true } : null}
      size={size}
      style={{ width: '120' }}
      placeholder={isLoading ? 'Loading' : isError ? 'Error' : 'Please select'}
      onChange={handleOnChange}
      options={options}
    />
  )

  function handleOnChange (newStock: string) {
    onchange(newStock)
  }
}
