import * as React from 'react'
import 'antd/dist/antd.css'
import { Story, Meta } from '@storybook/react'
import { HomeChart } from './HomeChart'
import { QueryClientProvider, QueryClient } from 'react-query'

export default {
  title: 'Screens/HomeChart',
  component: HomeChart,
  parameters: { actions: { argTypesRegex: '^on.*' } },
  decorators: [
    (Story) => {
      const queryClient = new QueryClient()
      return (
        <QueryClientProvider client={queryClient}>
          <Story />
        </QueryClientProvider>
      )
    }
  ]
} as Meta

export const Default: Story<Meta> = () => <HomeChart />
