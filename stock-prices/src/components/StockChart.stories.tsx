import * as React from 'react'
import 'antd/dist/antd.css'
import { Story, Meta } from '@storybook/react'
import { QueryClientProvider, QueryClient } from 'react-query'

import { StockChart } from './StockChart'

export default {
  title: 'Chart/TimeSeries',
  component: StockChart,
  decorators: [
    (Story) => {
      const queryClient = new QueryClient()
      return (
        <QueryClientProvider client={queryClient}>
          <Story />
        </QueryClientProvider>
      )
    }
  ]
} as Meta

export const Default: Story<Meta> = args => <StockChart {...args} />
Default.args = {
  stocks: ['AAPL']
}
